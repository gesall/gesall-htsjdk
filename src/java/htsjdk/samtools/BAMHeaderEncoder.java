package htsjdk.samtools;

import htsjdk.samtools.util.BinaryCodec;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.io.Writer;

//Author - Abhishek

public class BAMHeaderEncoder {
    private ByteArrayOutputStream baos;
    private BinaryCodec           outputBinaryCodec;
    private SAMFileHeader         samFileHeader;

    public BAMHeaderEncoder(SAMFileHeader samFileHeader) {
        this.samFileHeader = samFileHeader;
        baos = new ByteArrayOutputStream();
        outputBinaryCodec = new BinaryCodec();
        outputBinaryCodec.setOutputStream(baos);
    }

    public byte[] encode() {
        String headerText = getHeaderText(samFileHeader);

        outputBinaryCodec.writeBytes(BAMFileConstants.BAM_MAGIC);

        // calculate and write the length of the SAM file header text and the
        // header text
        outputBinaryCodec.writeString(headerText, true, false);

        // write the sequences binarily. This is redundant with the text header
        outputBinaryCodec
                .writeInt(samFileHeader.getSequenceDictionary().size());
        for (final SAMSequenceRecord sequenceRecord : samFileHeader
                .getSequenceDictionary().getSequences()) {
            outputBinaryCodec.writeString(sequenceRecord.getSequenceName(),
                    true, true);
            outputBinaryCodec.writeInt(sequenceRecord.getSequenceLength());
        }

        return baos.toByteArray();
    }

    private String getHeaderText(SAMFileHeader samFileHeader) {
        String headerString;
        Writer stringWriter = new StringWriter();
        new SAMTextHeaderCodec().encode(stringWriter, samFileHeader, true);
        headerString = stringWriter.toString();
        return headerString;
    }

}
