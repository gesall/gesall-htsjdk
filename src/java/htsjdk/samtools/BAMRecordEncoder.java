package htsjdk.samtools;

import htsjdk.samtools.util.BinaryCodec;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

// Author - Abhishek

public class BAMRecordEncoder {

    private ByteArrayOutputStream baos;
    private BinaryCodec           binaryCodec;
    private BinaryCigarCodec      cigarCodec;
    private BinaryTagCodec        binaryTagCodec;
    private SAMRecord alignment;

    public BAMRecordEncoder(SAMRecord alignment, int recordLengthEstimate) {
        this.alignment = alignment;
        baos = new ByteArrayOutputStream(recordLengthEstimate);
        binaryCodec = new BinaryCodec();
        binaryCodec.setOutputStream(baos);
        cigarCodec = new BinaryCigarCodec();
        binaryTagCodec = new BinaryTagCodec(binaryCodec);
    }

    public byte[] encode() {
        // Compute block size, as it is the first element of the file
        // representation of SAMRecord
        final int readLength = alignment.getReadLength();

        final int cigarLength = alignment.getCigarLength();

        int blockSize = BAMFileConstants.FIXED_BLOCK_SIZE
                + alignment.getReadNameLength() + 1 + // null terminated
                cigarLength * 4 + (readLength + 1) / 2 + // 2 bases per byte,
                                                         // round up
                readLength;

        final int attributesSize = alignment.getAttributesBinarySize();
        if (attributesSize != -1) {
            // binary attribute size already known, don't need to compute.
            blockSize += attributesSize;
        } else {
            SAMBinaryTagAndValue attribute = alignment.getBinaryAttributes();
            while (attribute != null) {
                blockSize += (BinaryTagCodec.getTagSize(attribute.value));
                attribute = attribute.getNext();
            }
        }

        int indexBin = 0;
        if (alignment.getReferenceIndex() >= 0) {
            if (alignment.getIndexingBin() != null) {
                indexBin = alignment.getIndexingBin();
            } else {
                indexBin = alignment.computeIndexingBin();
            }
        }

        // Blurt out the elements
        this.binaryCodec.writeInt(blockSize);
        this.binaryCodec.writeInt(alignment.getReferenceIndex());
        // 0-based!!
        this.binaryCodec.writeInt(alignment.getAlignmentStart() - 1);
        this.binaryCodec
                .writeUByte((short) (alignment.getReadNameLength() + 1));
        this.binaryCodec.writeUByte((short) alignment.getMappingQuality());
        this.binaryCodec.writeUShort(indexBin);
        this.binaryCodec.writeUShort(cigarLength);
        this.binaryCodec.writeUShort(alignment.getFlags());
        this.binaryCodec.writeInt(alignment.getReadLength());
        this.binaryCodec.writeInt(alignment.getMateReferenceIndex());
        this.binaryCodec.writeInt(alignment.getMateAlignmentStart() - 1);
        this.binaryCodec.writeInt(alignment.getInferredInsertSize());
        final byte[] variableLengthBinaryBlock = alignment
                .getVariableBinaryRepresentation();
        if (variableLengthBinaryBlock != null) {
            // Don't need to encode variable-length block, because it is
            // unchanged from when the record was read from a BAM file.
            this.binaryCodec.writeBytes(variableLengthBinaryBlock);
        } else {
            if (alignment.getReadLength() != alignment.getBaseQualities().length
                    && alignment.getBaseQualities().length != 0) {
                throw new RuntimeException(
                        "Mismatch between read length and quals length writing read "
                                + alignment.getReadName() + "; read length: "
                                + alignment.getReadLength()
                                + "; quals length: "
                                + alignment.getBaseQualities().length);
            }
            this.binaryCodec.writeString(alignment.getReadName(), false, true);
            final int[] binaryCigar = cigarCodec.encode(alignment.getCigar());
            for (final int cigarElement : binaryCigar) {
                // Assumption that this will fit into an integer, despite the
                // fact that it is specced as a uint.
                this.binaryCodec.writeInt(cigarElement);
            }
            this.binaryCodec.writeBytes(SAMUtils
                    .bytesToCompressedBases(alignment.getReadBases()));
            byte[] qualities = alignment.getBaseQualities();
            if (qualities.length == 0) {
                qualities = new byte[alignment.getReadLength()];
                Arrays.fill(qualities, (byte) 0xFF);
            }
            this.binaryCodec.writeBytes(qualities);
            SAMBinaryTagAndValue attribute = alignment.getBinaryAttributes();
            while (attribute != null) {
                this.binaryTagCodec.writeTag(attribute.tag, attribute.value,
                        attribute.isUnsignedArray());
                attribute = attribute.getNext();
            }
        }
        return baos.toByteArray();
    }

}
