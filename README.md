### About
This project is forked from HTSJDK. It has modifications such as to not split the reads across compressed BAM blocks, new classes to (de)serialize BAM records, etc.

### Building

##### Eclipse IDE
1. Import the code from `gesall-htsjdk` repository into Eclipse.
2. Add all the external JAR files from `gesall-htsjdk/lib` into `Project->Properties->Java Build Path->Libraries`.
3. Choose the `build.xml` from the. right click and select `Run As -> Ant Build` to generate the jars.

##### Command line 
1. Use the `build.xml` ant script to build the project : 
`$> ant`

Note: Since the ant file's name is build.xml , simply type `> ant` [enter], `> ant build` will give an error

### License

All copyright of Gesall code reserved by authors. This project includes code from HTSJDK <https://github.com/samtools/htsjdk> project.
